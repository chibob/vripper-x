'use strict'

require('./styles/app.css')
const React = require('react')
const cn = require('./cn')
const store = require('./clientStore')
const NewTaskForm = require('./NewTaskForm')
const TaskList = require('./TaskList')
const Actions = require('./Actions')
const Settings = require('./Settings')
const Preview = require('./Preview')

class App extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
      fullSettings: false,
      preview: null
    }
    this.setPreview = this.setPreview.bind(this)
    this.toggleSettings = this.toggleSettings.bind(this)
  }

  toggleSettings () {
    this.setState({
      fullSettings: !this.state.fullSettings
    })
  }

  setPreview (target) {
    this.setState({
      preview: target,
      fullSettings: false
    })
  }

  render () {
    return (
      <div className='app'>
        <div className='app_main'>
          <div className='app_main_controls'>
            <NewTaskForm />
          </div>
          <div className='app_main_tasks'>
            <TaskList setPreview={this.setPreview} />
          </div>
          <div className='app_main_actions'>
            <Actions />
          </div>
        </div>
        <div className='app_side'>
          <div className={cn('app_side_settings', { expanded: this.state.fullSettings })}>
            <Settings
              isToggled={this.state.fullSettings}
              toggle={this.toggleSettings}
              data={store._}
              update={store.setData}
            />
          </div>
          {!this.state.fullSettings &&
            <div className='app_side_preview'>
              <Preview target={this.state.preview} />
            </div>
          }
        </div>
      </div>
    )
  }
}

module.exports = App
